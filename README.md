# TALLER "BOTS LITERARIOS"

![Cartel del taller](./imagenes/presentacion.png)

Éste es el repositorio del taller "Bots literarios: Poesía narrativa por procedimientos en Tracery" que se imparte en el Centro de Cultura Digital. Ciudad de México. Noviembre, 2019

Imparte: *Daniel Martínez*

En el contenido está:

- La presentación de la primera sesión (carpeta "presentación")
- Algunos PDF importantes (carpeta "lecturas")
- Algunos ejemplos de gramáticas creados en clase (carpeta "ejemplos")
- Imágenes

**El pad del taller es**

https://pad.disroot.org/p/taller_bots_literarios

---

## Documentación Tracery

Tutorial de Allison Parrish

http://air.decontextualize.com/tracery/

Página oficial de Tracery

http://www.tracery.io/

GIT de Tracery

https://github.com/galaxykate/tracery

## Editores para Tracery

- https://beaugunderson.com/tracery-writer/
- http://brightspiral.com/tracery/
- http://tracery.io/editor/

## Tutoriales

- http://air.decontextualize.com/tracery/
- http://www.crystalcodepalace.com/traceryTut.html
- http://www.80grados.net/como-crear-un-robot-de-protesta-en-4-pasos/
- https://programminghistorian.org/en/lessons/intro-to-twitterbots
- https://dzone.com/articles/creating-your-own-whimsical-twitter-bot-with-trace 

## Tutorial para imágenes (en SVG)

https://github.com/derekahmedzai/cheapbotsdonequick/blob/master/svg-tracery-image-bots.md

---

## Redes sociales para el bot

### Mastodon

- https://joinmastodon.org/
- https://mastodon.social/
- https://youtu.be/IPSbNdBmWKE

Motor del bot

https://cheapbotstootsweet.com/

Tutoriales

- https://dev.to/botwiki/introduction-to-mastodon-bots-hfn
- https://botwiki.org/resource/tutorial/how-to-make-a-mastodon-bot-the-definitive-guide/

### Twitter

- https://twitter.com/
- https://developer.twitter.com
- https://developer.twitter.com/en/apps

Motor del bot

https://cheapbotsdonequick.com/

Para integrar Mastodon con Twitter

- https://crossposter.masto.donte.com.br/
- https://github.com/renatolond/mastodon-twitter-poster

---

## Páginas que aparecieron en la presentación

- http://www.80grados.net/como-crear-un-robot-de-protesta-en-4-pasos/
- https://es.wikipedia.org/wiki/Bot
- https://es.wikipedia.org/wiki/Problema_del_caballo
- https://es.wikipedia.org/wiki/Gram%C3%A1tica_libre_de_contexto
- https://www.youtube.com/watch?v=8IjURjPRIqk

---

## Bots creados en el taller 

**En Twitter**

- https://twitter.com/Animalesmuerto1 (bot de ejemplo)
- https://twitter.com/NostalgiaHipst1
- https://twitter.com/BotMelquiades
- https://twitter.com/chilanbotpolis
- https://twitter.com/ComoMurio (Aún en construcción)
- https://twitter.com/bot_cholo (Cholo-Bot)
- https://twitter.com/higado_el
- https://twitter.com/abotlina (Abotlina_Lesper)
- https://twitter.com/Bestiarium2

**En Mastodon**

https://mastodon.social/web/accounts/1002119
https://mastodon.social/web/accounts/1002117 (Abotlina_Lesper)
https://mastodon.social/@nostalgi_art_hispter
https://mastodon.social/web/accounts/1002121
https://mastodon.social/@abominable_higado_freson
https://mastodon.social/web/accounts/1002120 ( ... construcción)
@Lem20@mastodon.social
https://mastodon.social/web/accounts/1002125 (Alt Li Pixeled Girl)
https://mastodon.social/web/accounts/1002124 
https://artalley.social/web/accounts/147562 (Cholo-Bot)

---

## Bots de ejemplo interesantes

Les dejo aquí una colección de bots para que se inspiren cuando hagan el suyo.

### En español

- https://twitter.com/botdelcuerpo
- https://twitter.com/bot_de_pelis
- https://twitter.com/Poesia_es_Bot
- https://twitter.com/Poesia_noes_bot
- https://twitter.com/porcadapalabra
- https://twitter.com/UC_Poesias_Bot
- https://twitter.com/uneusuarie
- https://twitter.com/botliterario1
- https://twitter.com/bot_de_colores
- https://twitter.com/sinestesiabot
- https://twitter.com/bot_teorico
- https://twitter.com/KadVeth
- https://twitter.com/largo_aliento
- https://twitter.com/plots_esp
- https://twitter.com/Randompoetry4
- https://twitter.com/Calaveritamexi1
- https://twitter.com/JigokuShonen1
- https://twitter.com/Eugenio54168841

### En inglés:

- https://twitter.com/BestOfTheBots
- https://twitter.com/botsrcool
- https://twitter.com/MythologyBot
- https://twitter.com/vg_erotica
- https://twitter.com/garlicBreadBot
- https://twitter.com/botscurae
- https://twitter.com/LostTesla
- https://twitter.com/ThanetGuide
- https://twitter.com/str_voyage
- https://twitter.com/brandnewplanet
- https://twitter.com/MagicRealismBot
- https://twitter.com/BotCarrion
- https://twitter.com/RoofSlappingBot

### Bots gráficos

- https://twitter.com/happyautomata
- https://twitter.com/porcadadiagrama
- https://twitter.com/brutal_exe
- https://twitter.com/everyonesai
- https://twitter.com/tiny_star_field
- https://twitter.com/tiny_astro_naut
- https://twitter.com/tiny_mssn_ctrl
- https://twitter.com/digital_henge
- https://twitter.com/tiny_space_poo
- https://twitter.com/NiteAlps
- https://twitter.com/all_in_cards
- https://twitter.com/tiny_seas
- https://twitter.com/tiny_forests
- https://twitter.com/unchartedatlas
- https://twitter.com/choochoobot
- https://twitter.com/BracketMemeBot

Aquí está una lista que yo hice para ver a todos esos bots al mismo tiempo (y si voy encontrando otros, los agrego)

https://twitter.com/JackEliand/lists/bots

---

## Páginas importantes

Página de Kate Compton

https://www.galaxykate.com/

Página de Allison Parrish

http://www.decontextualize.com/

Motor de la presentación (Reveal.js)

https://github.com/hakimel/reveal.js

Centro de Cultura Digital

https://www.centroculturadigital.mx/

## Sobre "creatividad computacional"

Association for Computational Creativity 

(Tienen varios libros interesantes y hacen un congreso al año. El de 2020 es en Portugal)

http://computationalcreativity.net/home/

Coloquio Internacional en Creatividad Computacional

(La página del CICC de la UAN Cuajimalpa. Es la página del investigador Rafael Perez y Perez)

http://www.rafaelperezyperez.com/coloquio-internacional-creatividad-computacional/

Sobre la generción automática de narrativas (y el bot MEXICA)

https://www.youtube.com/watch?v=jNqvYOtyQJ4

---

## Contacto

https://twitter.com/jackeliand

---

**Prácticas de escritura digital**

https://drive.google.com/file/d/1FYGk8pawSZoQe2R-4oiv06HqVYBRdC0w/view?usp=sharing

![Prácticas de escritura digital](./imagenes/poster_practicas.png)

